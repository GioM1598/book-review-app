from django.shortcuts import render

# Create your views here.
from review.models import Review


def list_reviews(request):
    return render(request, "reviews/main.html")
    reviews = Review.object.all()
    context = {
        "reviews" : reviews,
    }
    return render(request, "reviews/main.html", context)